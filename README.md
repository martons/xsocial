# XSocial

This is a sample project to illustrate how you can enhance SharePoint 2013 social features. This project has been created to support my session in the European SharePoint Conference that is taking place in Barcelona from 5th to 8th of May 2014.

Please notice this is just for demo purposes and it doesn't pretend to be a guide on best practices.

Here you will see how to use _AngularJS_ and the _SharePoint 2013 REST API_.

The solution should be deployed to the web application containing the personal sites of your SharePoint 2013 farm. It will create two auxiliar lists:

* Uploads: to store post attachments
* Gamification: to act as a secondary datastore for all information that can't be added to the SharePoint feed

In the future I will update this page to give you more information on how to use this project.

This code is provided AS IS. Feel free to use it and change it as your convenience.
