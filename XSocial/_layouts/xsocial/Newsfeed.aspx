﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Newsfeed.aspx.cs" Inherits="XSocial._layouts.xsocial.Newsfeed" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

    
    <link rel="stylesheet" href="styles/basic-bnw.css"/>
    <link rel="stylesheet" href="styles/awesomic.css"/>
    <link rel="stylesheet" href="fancybox/jquery.fancybox.css"/>


    <script type="text/javascript" src="scripts/jquery-1.11.0.min.js"></script>

    <script type="text/javascript" src="scripts/angular.min.js"></script>
    <script type="text/javascript" src="scripts/angular.sanitize.js"></script>
    <script type="text/javascript" src="scripts/feedcontroller.js"></script>
    <script type="text/javascript" src="scripts/moment.min.js"></script>
    <script type="text/javascript" src="scripts/underscore-min.js"></script>

    <!-- Multiple upload -->
    <script type="text/javascript" src="uploadify/jquery.uploadify.js"></script>
    <link rel="stylesheet" type="text/css" href="uploadify/uploadify.css" />

    <script type="text/javascript" src="scripts/notify.min.js"></script>

        <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>

    
    <script type="text/javascript" src="scripts/autolinker.js"></script>
        <script type="text/javascript" src="scripts/embedly.min.js"></script>

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div ng-app="extendedNewsFeedApp">

        <div id="LeftColumn">
            <div ng-controller="microblogCtrl" data-ng-init="init()">

                <div class="MicroblogContainer">
                    <div class="left">
                        <img ng-src="{{profile.ImageUri}}" />
                    </div>
                    <div class="right">
                        <textarea ng-model="post.text" placeholder="Share something with your colleagues..."></textarea>
                        <div class="tools">
                            <input id="fileUpload" type="file">
                            <input value="Share" type="button" ng-click="addPost($event)" />
                        </div>
                    </div>
                    <div class="clearer"></div>
                </div>

            </div>

            <ul ng-controller="feedCtrl" class="threads" data-ng-init="init()" display-videos="{{1000}}">
                <li class="thread" ng-repeat="thread in threads">
                    <div class="left">
                        <img class="owner" ng-src="{{thread.Owner.ImageUri}}" />
                    </div>
                    <div class="right">
                        <div class="name" ng-bind="thread.Owner.Name"></div>
                        <p ng-bind-html="thread.Text" class="text"></p>
                        <div class="attachments" ng-hide="thread.Attributes == 0">
                            <ul class="attachments">
                                <li class="attachment" ng-repeat="attachment in thread.attachments">
                                    <a class="fancybox fancybox-thumb" rel="fancybox-thumb" ng-href="{{attachment.ImageUri}}"><img ng-src="{{attachment.ImageUri}}"/></a>
                                </li>
                            </ul>
                            <div class="clearer"></div>
                        </div>
                        <div class="likes" ng-show="thread.LikesCount > 0">
                            {{thread.LikesCount}} Likes
                        </div>
                        <div class="created">{{thread.CreatedTime | fromNow}}</div>
                        <div class="tools" ng-hide="thread.Attributes == 0">
                            <a href ng-click="show = !show" ng-show="thread.replies.length == 0">Reply</a>
                            <a href ng-click="like($event, thread.Id)" ng-hide="thread.LikedByCurrentUser">Like</a>
                            <a href ng-click="unlike($event, thread.Id)" ng-show="thread.LikedByCurrentUser">Unlike</a>
                            <a href ng-click="delete($event, thread.Id)">Delete</a>
                        </div>
                        <div class="clearer"></div>
                        <div ng-show="thread.RepliesCount > 2  && thread.replies.length < 3"><a href ng-click="getPost($event, thread.Id)">Show total <span ng-bind="thread.RepliesCount"></span>replies</a></div>
                        <ul class="replies">
                            <li class="reply" ng-repeat="reply in thread.replies">
                                <div class="left">
                                    <img class="owner" ng-src="{{reply.Owner.ImageUri}}" />
                                </div>
                                <div class="right">
                                    <div class="name">{{reply.Owner.Name}}</div>
                                    <p class="text">{{reply.Text}}</p>
                                    <div class="attachments">
                                        <ul class="attachments">
                                            <li class="attachment" ng-repeat="attachment in reply.attachments">
                                                <a class="fancybox fancybox-thumb" rel="fancybox-thumb" ng-href="{{attachment.ImageUri}}"><img ng-src="{{attachment.ImageUri}}"/></a>
                                            </li>
                                        </ul>
                                        <div class="clearer"></div>
                                    </div>
                                    <div class="likes" ng-show="reply.LikesCount > 0">
                                        {{reply.LikesCount}} Likes
                                    </div>
                                    <div class="created">{{reply.CreatedTime | fromNow}}</div>
                                    <div class="tools">
                                        <a href ng-click="like($event, reply.Id)" ng-hide="reply.LikedByCurrentUser">Like</a>
                                        <a href ng-click="unlike($event, reply.Id)" ng-show="reply.LikedByCurrentUser">Unlike</a>
                                        <a href ng-click="delete($event, reply.Id)">Delete</a>
                                    </div>
                                </div>
                                <div class="clearer"></div>
                            </li>
                        </ul>
                        <div class="replyButton" ng-show="thread.replies.length > 0 || show">
                            <textarea ng-model="thread.replyText" placeholder="Reply here..."></textarea>
                            <input type="button" value="Reply" ng-click="addReply($event, thread.Id, thread.replyText)" />
                        </div>
                    </div>

                    <div class="clearer"></div>

                </li>
            </ul>



        </div>
        <div id="RightColumn">
            <div ng-controller="xsocialCtrl" data-ng-init="init()">
                <h1>Quick stats</h1>
                Your current points: <span ng-bind="contributions.Points" id="currentPoints"></span><br/>
                <span ng-show="topContributors[0].profile.AccountName == profile.AccountName">You are the top contributor!!!</span><br />
                <span ng-show="topContributors[0].profile.AccountName != profile.AccountName">You need <span ng-bind="topContributors[0].points - contributions.Points"></span> more points to be the top contributor!!</span>
                <br/>
                Posts created: <span ng-bind="contributions.numberOfPostedMessages"></span><br/>
                Posts liked: <span ng-bind="contributions.numberOfLikes"></span>
                <br />
                <div>
                    <h1 ng-show="contributions.notifications.length > 0">Your notifications</h1>
                    <ul class="notifications">
                        <li ng-repeat="notification in contributions.notifications">
                            <div class="left">
                                <img ng-src="{{notification.user.ImageUri}}" title="{{notification.user.Name}}" />
                            </div>
                            
                            <div class="right"><span ng-bind="notification.user.Name"></span> <span ng-bind="notification.message"></span><br/> <span ng-bind="notification.related"></span></div>
                            <div class="clearer"></div>
                        </li>
                    </ul>
                </div>
                <div>
                    <h1 ng-show="topContributors.length > 0">Top contributors</h1>
                    <div class="contributors">
                        <ul class="contributors">
                            <li ng-repeat="contributor in topContributors">
                                <img ng-src="{{contributor.profile.ImageUri}}" title="{{contributor.profile.Name}}" />
                                <div ng-bind="contributor.points"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearer"></div>
                </div>
                <div>
                    <h1>Your missions</h1>
                    <ul class="missions">
                        <li ng-repeat="mission in missions">
                            <span ng-href="{{mission.link}}" ng-class="{ finished : contributions.achievements != null && contributions.achievements.indexOf(mission.id) != -1 }" ng-bind="mission.title"></span>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    XNewsfeed
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    XSocial
</asp:Content>

