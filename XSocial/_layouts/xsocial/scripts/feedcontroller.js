﻿
angular.module('extendedNewsFeedApp', ['ngSanitize'])
    .factory('_', function() {
        return window._; // assumes underscore has already been loaded on the page
    })
    .filter('fromNow', function() {
        return function(date) {
            return moment(date).fromNow();
        };
    })

    //.directive('displayVideos', [
    //    '$timeout', function (timer) {
    //        return {
    //            link: function () {
    //                var hello = function () {
    //                    alert("hola");
    //                    $('p.text > a').embedly({
    //                        key: 'abbf1418648c4368bbda8a1d08221a81',
    //                        query: { maxwidth: 530 }
    //                    });

    //                }
    //                timer(hello, 1000);
    //            }
    //        }
    //    }
    //])
    .service('messageBus', function($rootScope) {
        return {
            retrieve: function () {
                $rootScope.$broadcast('feedRetrieved');
            },
            post: function (thread) {
                $rootScope.$broadcast('messagePosted', thread);
            },
            like: function(thread) {
                $rootScope.$broadcast('postLiked', thread);
            },
            reply: function (thread) {
                $rootScope.$broadcast('postReplied', thread);
            }, profile: function (user) {
                $rootScope.$broadcast('profileLoaded', user);
            }
        };
    })
    .controller('microblogCtrl', function($scope, $http, messageBus, _) {

        function initializeUploadify() {
            $("#fileUpload").uploadify({
                height: 30,
                removeTimeout: 0,
                swf: _spPageContextInfo.siteAbsoluteUrl + '/_layouts/15/xsocial/uploadify/uploadify.swf',
                uploader: _spPageContextInfo.siteAbsoluteUrl + '/_layouts/15/xsocial/upload.aspx',
                width: 120,
                'onUploadComplete': function (file) {
                    if (!$scope.Attachments) {
                        $scope.Attachments = [];
                    }
                    $scope.Attachments.push(_spPageContextInfo.siteAbsoluteUrl + "/lists/uploads/" + file.name);
                },
                removeCompleted: false,
                multiple: true
            });
        }

        // Multiple upload
        $(function () {
            initializeUploadify();
        });

        $scope.init = function() {
            // Get current user profile
            $http({
                method: "GET",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/my",
                headers: {
                    "Accept": "application/json;odata=verbose"
                }
            }).success(function (data) {
                $scope.profile = data.d.Me;

                if ($scope.profile.ImageUri === null) {
                    $scope.profile.ImageUri = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/images/PersonPlaceholder.42x42x32.png?rev=23";
                }
                messageBus.profile($scope.profile);
            }).error(function() {
                alert("There was an error trying to get the user's profile.");
            });
        };

        $scope.addPost = function($event) {
            $event.preventDefault();

            $("#fileUpload").uploadify('destroy');
            initializeUploadify();

            var text = $scope.post.text;

            var contentItems = new Array();
            var attachment = null;
            if ($scope.Attachments != null) {
                attachment = {
                    '__metadata': {
                        'type': 'SP.Social.SocialAttachment'
                    },
                    AttachmentKind: 0,
                    Uri: $scope.Attachments[0],
                    Name: $scope.Attachments[0]
                };

                for (var i = 1; i < $scope.Attachments.length; i++) {

                    contentItems.push({
                        '__metadata': {
                            'type': 'SP.Social.SocialDataItem'
                        },
                        'Uri': $scope.Attachments[i],
                        'ItemType': 4
                    });

                    text = text + " {" + (i - 1) + "}";

                }
            }

            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/my/Feed/Post",
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": getFormDigest()
                },
                data: JSON.stringify({
                    'restCreationData': {
                        '__metadata': {
                            'type': 'SP.Social.SocialRestPostCreationData',
                        },
                        'ID': null,
                        'creationData': {
                            '__metadata': {
                                'type': 'SP.Social.SocialPostCreationData',
                            },
                            'Attachment': attachment,
                            'ContentItems': { 'results': contentItems },
                            'ContentText': text,
                            'UpdateStatusText': false
                        }
                    }
                }),
            }).success(function(data) {
                $scope.post.text = "";
                $scope.Attachments = null;
                messageBus.post(convert(data.d.SocialThread));
            }).error(function(xhr) {
                alert(xhr.status + ': ' + xhr.statusText);
            });

        }
    })
    .controller('feedCtrl', function($scope, $http, messageBus, _) {


        $scope.$on('profileLoaded', function (event, user) {
            $scope.profile = user;
        });

        $scope.$on('messagePosted', function(event, x) {
            getFeed();
        });

        $scope.$on('feedRetrieved', function (event, x) {
            setTimeout(function () {
                $('p.text > a').embedly({
                    key: 'abbf1418648c4368bbda8a1d08221a81',
                    query: { maxwidth: 530 }
                });
            }, 500);
        });

        $scope.init = function() {

            // Get current user profile
            $http({
                method: "GET",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/my",
                headers: {
                    "Accept": "application/json;odata=verbose"
                }

            }).success(function(data) {
                $scope.me = data.d.Me;

                if ($scope.me.ImageUri == null) {
                    $scope.me.ImageUri = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/images/PersonPlaceholder.42x42x32.png?rev=23";
                }

            }).error(function() {
                alert("There was an error trying to get the user's profile.");
            });

            // Get current user feed
            getFeed();
        };

        function getFeed() {
            $http({
                method: "GET",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/my/timelinefeed(MaxThreadCount=10,SortOrder=1)",
                headers: {
                    "Accept": "application/json;odata=verbose"
                }

            }).success(function(data) {
                var threads = data.d.SocialFeed.Threads.results;

                $scope.threads = [];
                for (var i = 0; i < threads.length; i++) {
                    $scope.threads.push(convert(threads[i]));
                }

                $(".fancybox-thumb").fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    helpers: {
                        title: {
                            type: 'outside'
                        },
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });

                messageBus.retrieve();

            }).error(function() {
                alert("There was an error trying to get the user's feed.");
            });

        }

        $scope.reply = function($event, id) {
            $event.preventDefault();

            for (var i = 0; i < threads.length; i++) {
                var thread = threads[i];
                if (thread.Id === id) {
                    $scope.threads[i].allowReply = true;
                    break;
                }
            }
        }

        $scope.addReply = function($event, id, text) {
            $event.preventDefault();

            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/post/reply",
                data: JSON.stringify({
                    'restCreationData': {
                        '__metadata': {
                            'type': 'SP.Social.SocialRestPostCreationData'
                        },
                        'ID': id,
                        'creationData': {
                            '__metadata': {
                                'type': 'SP.Social.SocialPostCreationData'
                            },
                            'ContentText': text,
                            'UpdateStatusText': false
                        }
                    }
                }),
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": getFormDigest()
                },

            }).success(function(data) {
                getFeed();
                messageBus.reply(convert(data.d.SocialThread));
            }).error(function() {
                alert("There was an error trying to reply to the post.");
            });
        }

        $scope.getPost = function($event, id) {
            $event.preventDefault();

            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/post",
                data: JSON.stringify({
                    'ID': id
                }),
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": getFormDigest()
                },

            }).success(function(data) {
                //getFeed();
                var threads = $scope.threads;
                for (var i = 0; i < threads.length; i++) {
                    var thread = threads[i];
                    if (thread.Id === id) {
                        $scope.threads[i] = convert(data.d.SocialThread);
                        break;
                    }
                }

            }).error(function() {
                alert("There was an error trying to like the post.");
            });
        }

        $scope.like = function($event, id) {
            $event.preventDefault();

            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/post/like",
                data: JSON.stringify({
                    'ID': id
                }),
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": getFormDigest()
                },

            }).success(function(data) {
                getFeed();
                messageBus.like(convert(data.d.SocialThread));
            }).error(function() {
                alert("There was an error trying to like the post.");
            });
        }

        $scope.unlike = function($event, id) {
            $event.preventDefault();

            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/post/unlike",
                data: JSON.stringify({
                    'ID': id
                }),
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": getFormDigest()
                }
            }).success(function(data) {
                getFeed();

            }).error(function() {
                alert("There was an error trying to like the post.");
            });
        }

        $scope.delete = function($event, id) {
            $event.preventDefault();

            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/social.feed/post/delete",
                data: JSON.stringify({
                    'ID': id
                }),
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": getFormDigest()
                }
            }).success(function(data) {
                getFeed();

            }).error(function() {
                alert("There was an error trying to like the post.");
            });
        }

    })
    .controller('xsocialCtrl', function ($scope, $http, _) {

        $scope.$on('profileLoaded', function (event, user) {

            $scope.profile = user;

            var contributions = getContributionsByUser(user.AccountName);
            $scope.contributions = contributions.Contributions;

            initializeGeneralContributions();

        });

        $scope.$on('messagePosted', function(event, message) {
            var contributions = getContributionsByUser(message.Owner.AccountName);
            addPoints(contributions.Contributions, 10);
            if (message.attachments.length > 0) {
                addAchievement(contributions.Contributions, 4);
            }
            addAchievement(contributions.Contributions, 1);
            contributions.Contributions.numberOfPostedMessages += 1;
            $scope.contributions = contributions.Contributions;
            updateTopContributors();
            saveContributions(contributions);
        });

        $scope.$on('postLiked', function (event, message) {

            console.log($scope.contributions);

            // Get contributions of message owner to add a notification
            if (message.Owner.AccountName !== $scope.profile.AccountName) {

                console.log("Updating message owner contributions");

                var contributions = getContributionsByUser(message.Owner.AccountName);
                console.log(contributions);

                if (!contributions.Contributions.notifications) {
                    contributions.Contributions.notifications = [];
                }

                contributions.Contributions.numberOfLikes += 1;

                var notification = {
                    user: $scope.profile,
                    message: "liked your post",
                    related: message.Text
                };

                contributions.Contributions.notifications.push(notification);

                addAchievement(contributions.Contributions, 5);

                saveContributions(contributions);
            }


            console.log("Updating current user contributions");
            var currentContributions = getContributionsByUser($scope.profile.AccountName);
            console.log(currentContributions);

            addAchievement(currentContributions.Contributions, 2);
            addPoints(currentContributions.Contributions, 5);
            saveContributions(currentContributions);

            $scope.contributions = currentContributions.Contributions;

            console.log($scope.contributions);

            updateTopContributors();
        });

        $scope.$on('postReplied', function (event, message) {

            console.log($scope.contributions);

            // Get contributions of message owner to add a notification
            if (message.Owner.AccountName !== $scope.profile.AccountName) {

                console.log("Updating message owner contributions");

                var contributions = getContributionsByUser(message.Owner.AccountName);
                console.log(contributions);

                if (!contributions.Contributions.notifications) {
                    contributions.Contributions.notifications = [];
                }

                contributions.Contributions.numberOfReplies += 1;

                var notification = {
                    user: $scope.profile,
                    message: "commented your post"
                };

                contributions.Contributions.notifications.push(notification);

                saveContributions(contributions);
            }


            console.log("Updating current user contributions");
            var currentContributions = getContributionsByUser($scope.profile.AccountName);
            console.log(currentContributions);

            addAchievement(currentContributions.Contributions, 3);
            addPoints(currentContributions.Contributions, 3);
            saveContributions(currentContributions);

            $scope.contributions = currentContributions.Contributions;

            console.log($scope.contributions);

            updateTopContributors();
        });

        function addAchievement(contributions, achievement) {
            if (contributions.achievements == null) {
                contributions.achievements = [];
            }

            if (contributions.achievements.indexOf(achievement) == -1) {
                contributions.achievements.push(achievement);
                showNotification("Achievement unlocked!!!");
            }
        }

        function addPoints(contributions, points) {
            contributions.Points += points;
            showNotification("+ " + points); showNotification("+ " + points);
        }


        function showNotification(text) {
            $('#currentPoints').notify(
                text,
                {
                    position: "right, middle",
                    clickToHide: true,
                    autoHide: true,
                    autoHideDelay: 2000,
                    arrowShow: false,
                    style: 'bootstrap',
                    className: 'success',
                    showAnimation: 'slideDown',
                    showDuration: 400,
                    hideAnimation: 'slideUp',
                    hideDuration: 200,
                    gap: 2
                }
            );
        }

    function initializeGeneralContributions() {
        var systemInfo = getContributionsByUser("xsocial");
        
        if (!systemInfo.Contributions.Missions) {
            systemInfo.Contributions.Missions = [
                { id: 1, title: "Create your first post.", link: "#" },
                { id: 2, title: "Like your first post.", link: "#" },
                { id: 3, title: "Reply your first post.", link: "#" },
                { id: 4, title: "Upload your first picture.", link: "#" },
                { id: 5, title: "Receive your first like.", link: "#" },
                { id: 6, title: "Reach the top contributor.", link: "#" }
            ];
        }
        if (!systemInfo.Contributions.TopContributors) {
            systemInfo.Contributions.TopContributors = [];
            systemInfo.Contributions.TopContributors.push({
                accountName: $scope.profile.AccountName,
                profile: $scope.profile,
                points: $scope.contributions.Points
            });
         }

        saveContributions(systemInfo);

        $scope.topContributors = systemInfo.Contributions.TopContributors;
        $scope.missions = systemInfo.Contributions.Missions;
    }

    function updateTopContributors() {
        var systemInfo = getContributionsByUser("xsocial");
        if (!systemInfo.Contributions.Missions) {
            systemInfo.Contributions.Missions = [
                { id: 1, title: "Create your first post.", link: "#"},
                { id: 2, title: "Like your first post.", link: "#" },
                { id: 3, title: "Reply your first post.", link: "#" },
                { id: 4, title: "Upload your first picture.", link: "#" },
                { id: 5, title: "Receive your first like.", link: "#" },
                { id: 6, title: "Rich the top contributor.", link: "#" }
            ];
        }
        if (!systemInfo.Contributions.TopContributors) {
            systemInfo.Contributions.TopContributors = [];
            systemInfo.Contributions.TopContributors.push({
                accountName: $scope.profile.AccountName,
                profile: $scope.profile,
                points: $scope.contributions.Points
            });
        } else {

            var includedInTopContributors = _.find(systemInfo.Contributions.TopContributors, function (contributor) { return contributor.profile.AccountName == $scope.profile.AccountName; });

            if (includedInTopContributors) {
                includedInTopContributors.points = $scope.contributions.Points;
            }
            else {
                systemInfo.Contributions.TopContributors.push({
                    accountName: $scope.profile.AccountName,
                    profile: $scope.profile,
                    points: $scope.contributions.Points
                });
            }

            systemInfo.Contributions.TopContributors = _.first(_.sortBy(systemInfo.Contributions.TopContributors, function (contributor) { return contributor.points * -1; }), 3);

            if (systemInfo.Contributions.TopContributors[0].accountName == $scope.profile.AccountName) {
                addAchievement($scope.contributions, 6);
            }
        }

        saveContributions(systemInfo);

        $scope.topContributors = systemInfo.Contributions.TopContributors;
        $scope.missions = systemInfo.Contributions.Missions;

    }

    function getContributionsByUser(loginName) {
            var contributions = null;

            $.ajax({
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('gamification')/items()/?$filter=Title eq '" + loginName + "'",
                type: "GET",
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose"
                },
                dataType: "json",
                success: function(data) {
                    if (data.d && data.d.results.length == 1) {
                        contributions = {
                            'Id': data.d.results[0].Id,
                            'Title': data.d.results[0].Title,
                            'Contributions': JSON.parse(data.d.results[0].Contributions)
                        }
                    } else {

                        contributions = {
                            Title: loginName,
                            Contributions: {
                                Points: 1,
                                numberOfPostedMessages: 0,
                                numberOfLikes : 0
                            }
                        }

                        createContributions(contributions);
                    }
                },
                error: function(xhr) {
                    console.error(xhr.status + ': ' + xhr.statusText);
                },
                async: false
            });

            return contributions;
        }

        function createContributions(contributions) {
            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('gamification')/items",
                headers: {
                    "content-type": "application/json;odata=verbose",
                    "accept": "application/json; odata=verbose",
                    "X-RequestDigest": getFormDigest(),
                    "If-Match": "*"
                },
                data: {
                    '__metadata': { 'type': 'SP.Data.GamificationListItem' },
                    'Title': contributions.Title,
                    'Contributions': JSON.stringify(contributions.Contributions)
                },
            }).success(function(data2) {
                var a = data2;

            }).error(function(error) {
                alert("There was an error trying to like the post.");
            });
        }

        function saveContributions(contributions) {
            $http({
                method: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('gamification')/items(" + contributions.Id + ")",
                headers: {
                    "content-type": "application/json;odata=verbose",
                    "accept": "application/json; odata=verbose",
                    "X-RequestDigest": getFormDigest(),
                    "If-Match": "*",
                    "X-HTTP-Method": "MERGE"
                },
                data: {
                    '__metadata': { 'type': 'SP.Data.GamificationListItem' },
                    'Title': contributions.Title,
                    'Contributions': JSON.stringify(contributions.Contributions)
                },
            }).success(function(data2) {
                

            }).error(function(error) {
                console.log("Error saving contributions", JSON.stringify(error));
            });
        }
    });

    function getFormDigest() {
        var digest = '';

        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/contextinfo",
            type: "POST",
            headers: { "accept": "application/json; odata=verbose" },
            dataType: "json",
            success: function (data) {
                if (data.d) {
                    digest = data.d.GetContextWebInformation.FormDigestValue;
                    console.log("FormDigest success");
                }
            },
            error: function (xhr) {
                console.error(xhr.status + ': ' + xhr.statusText);
            },
            async: false
        });

        return digest;
    }

    function convert(thread) {
        var result = {};


        var post = thread.RootPost;

        result.Id = post.Id;
        result.Attributes = thread.Attributes;
        result.Text = Autolinker.link(post.Text);
        result.CreatedTime = post.CreatedTime;
        result.PreferredImageUri = post.PreferredImageUri;
        result.Owner = {};
        result.Owner.AccountName = thread.Actors.results[thread.OwnerIndex].AccountName;
        result.Owner.ImageUri = thread.Actors.results[thread.OwnerIndex].ImageUri;
        if (result.Owner.ImageUri == null) {
            result.Owner.ImageUri = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/images/PersonPlaceholder.42x42x32.png?rev=23";
        }
        result.Owner.Name = thread.Actors.results[thread.OwnerIndex].Name;

        // Replies
        result.RepliesCount = thread.TotalReplyCount;
        result.replies = [];
        for (var i = 0; i < thread.Replies.results.length; i++) {
            var r = thread.Replies.results[i];
            var reply = {};
            reply.Id = r.Id;
            reply.Text = Autolinker.link(r.Text);
            reply.CreatedTime = r.CreatedTime;
            reply.Owner = {};
            reply.Owner.AccountName = thread.Actors.results[r.AuthorIndex].AccountName;
            reply.Owner.ImageUri = thread.Actors.results[r.AuthorIndex].ImageUri;
            if (reply.Owner.ImageUri == null) {
                reply.Owner.ImageUri = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/images/PersonPlaceholder.42x42x32.png?rev=23";
            }
            reply.Owner.Name = thread.Actors.results[r.AuthorIndex].Name;

            reply.attachments = [];
            if (r.Attachment) {
                reply.attachments.push({ ImageUri: r.Attachment.Uri });
            }

            if (r.LikerInfo !== null) {
                reply.LikesCount = r.LikerInfo.TotalCount;
                reply.LikedByCurrentUser = r.LikerInfo.IncludesCurrentUser;
            }

            result.replies.push(reply);
        }

        // Likes
        if (post.LikerInfo !== null) {
            result.LikesCount = post.LikerInfo.TotalCount;
            result.LikedByCurrentUser = post.LikerInfo.IncludesCurrentUser;
        }


        // Attachments
        result.attachments = [];
        if (post.Attachment) {
            result.attachments.push({ ImageUri: post.Attachment.Uri });
        }

        var parsed = result.Text;

        if (thread.RootPost.Overlays != null) {

            var overlays = post.Overlays.results;
            if (overlays.length > 0) {
                for (var k = post.Overlays.results.length - 1; k >= 0; k--) {
                    var overlay = post.Overlays.results[k];
                    result.attachments.push({ ImageUri: overlay.LinkUri });
                    var sufix = parsed.substr(overlay.Index + overlay.Length);
                    var prefix = parsed.substr(0, overlay.Index);
                    if (overlay.OverlayType == 0) {
                        var title = parsed.substr(overlay.Index, overlay.Length);
                        parsed = prefix + "<a href='" + overlay.LinkUri + "'>" + title + "</a>" + sufix;
                    }
                    //else {
                    //    for (var j = 0 ; j < overlay.ActorIndexes.results.length; j++) {
                    //        var actor = overlay.ActorIndexes.results[j];
                    //        var obj = participants.results[overlay.ActorIndexes.results[j]];
                    //        parsed = prefix + "<a href='" + obj.Uri + "'>" + obj.Name + "</a>" + sufix;
                    //    }
                    //}
                }
            }

        }

        result.Text = parsed;


        return result;
    }