﻿namespace XSocial._layouts.xsocial
{
    using System;
    using System.IO;
    using System.Security.Permissions;
    using System.Web;
    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Utilities;
    using Microsoft.SharePoint.WebControls;
    
    public partial class Upload : LayoutsPageBase
    {
        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.RequestType == "POST")
            {
                AddDocument();
            }
        }

        private void AddDocument()
        {
            try
            {
                var filenameNotIe = HttpContext.Current.Request.Headers["X-File-Name"];

                if (Request.Files.Count > 0 || !string.IsNullOrEmpty(filenameNotIe))
                {
                    Stream fStream;
                    string filename;
                    if (string.IsNullOrEmpty(filenameNotIe))
                    {
                        var file = Request.Files[0];
                        filename = Path.GetFileName(file.FileName);
                        fStream = file.InputStream;
                    }
                    else
                    {
                        fStream = Request.InputStream;
                        filename = filenameNotIe;
                    }

                    var web = SPContext.Current.Web;

                    var resultUrl = SPUrlUtility.CombineUrl(SPContext.Current.Web.ServerRelativeUrl, "lists/uploads/" + filename);


                    web.AllowUnsafeUpdates = true;
                    var uploaded = web.Files.Add(resultUrl, fStream, new SPFileCollectionAddParameters { Overwrite = true });

                    if (uploaded.Item.ParentList.ForceCheckout)
                    {
                        uploaded.CheckIn("Automatic check in", SPCheckinType.MajorCheckIn);
                    }

                    web.AllowUnsafeUpdates = false;
                    string documentPath = uploaded.ServerRelativeUrl;


                    // "{"success": true, "filename": "/lists/uploads/file.png"}"
                    // important to return true success for ajax upoad to act correctly
                    // can also wrap above in try catch and return error in catch
                    WriteResponse("{\"success\": true, \"url\": \"" + documentPath + "\", \"filename\": \"" + filename + "\"}");
                }
                else
                {
                    WriteResponse("{\"success\": false}");
                }

            }
            catch (Exception ex)
            {
                WriteResponse("{\"success\": false, \"details\": \"" + ex + "\"}");
            }

        }

        private void WriteResponse(string output)
        {
            Response.Clear();
            Response.ContentType = "text/html";
            Response.Write(output);
            Response.Flush();
            Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}
