namespace XSocial.Features.Lists
{
    using System.Runtime.InteropServices;
    using Microsoft.SharePoint;

    [Guid("ce9f2c1e-886a-41a5-adef-b2661548a19e")]
    public class ListsEventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            /*
             * (For demo purposes only) As we are using the REST API we need to ensure everyone has write-access on these two lists
             */
            var web = properties.Feature.Parent as SPWeb;

            if (web != null)
            {
                var uploadsList = web.Lists["Uploads"];
                uploadsList.BreakRoleInheritance(true);

                var gamificationList = web.Lists["Gamification"];
                gamificationList.BreakRoleInheritance(true);

                var user = web.EnsureUser("Everyone");

                var definition = web.RoleDefinitions.GetByType(SPRoleType.Contributor);
                var roleAssignment = new SPRoleAssignment(user);
                roleAssignment.RoleDefinitionBindings.Add(definition);
                uploadsList.RoleAssignments.Add(roleAssignment);
                uploadsList.Update();

                gamificationList.RoleAssignments.Add(roleAssignment);
                gamificationList.Update();

            }
        }
    }
}
